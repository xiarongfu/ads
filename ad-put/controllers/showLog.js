let Models = require('../models')

module.exports = {
    log: function (request, reply) {
        var putIds = JSON.parse(request.payload.putIds);
        if(!putIds || putIds.length == 0){
            return '';
        } 
        
        for(var i = 0; i < putIds.length; i++){
            Models.ShowLog.create({
               put_id: putIds[i],
               from_ip: request.info.remoteAddress,
               from_url: request.payload.fromUrl,
               visitor: request.state.visitor.id
            });
            
            Models.sequelize.query(
                "UPDATE ads_put p \
                    SET p.show_count = p.show_count + 1, \
                        p.curr_show_count = IF(p.curr_date = DATE(SYSDATE()), p.curr_show_count + 1, 1), \
                        p.curr_date = IF(p.curr_date = DATE(SYSDATE()), p.curr_date, DATE(SYSDATE())) \
                  WHERE p.id = $id",
                {
                    bind: {id: putIds[i]}
                });      
        }
        return '';
    }
};