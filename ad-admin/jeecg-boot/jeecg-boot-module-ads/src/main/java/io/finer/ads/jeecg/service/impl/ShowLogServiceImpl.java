package io.finer.ads.jeecg.service.impl;

import io.finer.ads.jeecg.entity.ShowLog;
import io.finer.ads.jeecg.mapper.ShowLogMapper;
import io.finer.ads.jeecg.service.IShowLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 呈现日志-最新
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Service
public class ShowLogServiceImpl extends ServiceImpl<ShowLogMapper, ShowLog> implements IShowLogService {

}
